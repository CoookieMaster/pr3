import tkinter as ttk
from tkinter import messagebox


def raschet(a, b, c):
    summpr = 0
    summd = 0
    summpl = 0
    i = 1
    while i <= b:
        r = c / 100 / 12
        pr = a * c / 100 / 12
        AP = a * ((r * (1 + (r) ** b)) / ((1 + r) ** b - 1))
        if (i == 1):
            summ = pr + AP
        else:
            AP = summ - pr
        i += 1
        a -= AP
        summpr += pr
        summd += AP
        summpl += summ
    outputs = []
    outputs.append(summpr)
    outputs.append(summd)
    outputs.append(summpl)
    return outputs


def calc():
    # !!!!!!!!!!!!!!!!!!!!!!!!Не считает дальше
    a = int(text1.get())
    b = int(text2.get())
    c = int(text3.get())
    r = raschet(a, b, c)
    outputs = f'Вывод: '  # {str(r)}'
    messagebox.showinfo(title='Посчитать',
                        message=outputs + "\n" + "Проценты: " + str(r[0]) + ",\nОсновной долг: " + str(r[1]) +
                                ", \nСумма платежа: " + str(r[2]))


def main():
    root.title("Loan Calculator")
    root.geometry("400x300")
# Labels+text
    l1 = Label(root, text="Введите сумму кредита:", width=25, height=2)
    l1.pack()

    text1.pack()
    l2 = Label(root, text="Введите кол-во месяцев:", width=25, height=2)
    l2.pack()

    text2.pack()
    l3 = Label(root, text="Введите % годовых:", width=25, height=2)
    l3.pack()

    text3.pack()
    btn = Button(root, text='Рассчитать', bg='white', command=calc)
    btn.pack()

    root.mainloop()


if __name__ == '__main__':
   root = Tk()
   text1 = Entry(root, width=10, text='Посчитать')
   text2 = Entry(root, width=10)
   text3 = Entry(root, width=10)
   main()
